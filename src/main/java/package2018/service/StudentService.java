package package2018.service;


import package2018.interfaces.ScopedEntityManager;

import package2018.managers.PersistenceManager;

import java.util.List;

import package2018.entities.Student;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;





@Stateless
@LocalBean
@Path("/students")
public class StudentService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Student> getAllStudents(){
		try(ScopedEntityManager em = getScopedEntityManager()){
			
			
    		List<Student> Student = em.createQuery("select s from Student s").getResultList();
    		
			return Student;
		}
	}
	
	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Student getStudent(@PathParam("id") Long id) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		TypedQuery<Student> query = em.createQuery("select s from Student s where s.id = :id", Student.class);
    		query.setParameter("id", id.intValue());
    		
    		Student student = query.getSingleResult();
    		
			return student;
		}
	}
	 @DELETE
	    @Path("{id}")
	    @Produces(MediaType.APPLICATION_JSON)
	    public void deleteStudent(@PathParam("id") int id) {
	    	try (ScopedEntityManager em = getScopedEntityManager()) {
	    		
	    		EntityTransaction tx = em.getTransaction();

	    		tx.begin();
	    		
	    		Query query = em.createQuery("delete from Student where id = :id");
	    		
	    		query.setParameter("id", id).executeUpdate();
	    		
	    		tx.commit();
	    		
	    	}
	    }
	 
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void createStudent(Student student) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();

    		em.persist(student);
    		
    		tx.commit();
    		
    	}
    }
    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateStudent(@PathParam("id") int id, Student student) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();
    		
    		
    		Query query = em.createQuery("update Student s set"
    				+ "  s.studentname = :studentname"
    				+ "  where s.id = :id");
    		
    		query.setParameter("id", id);
    		query.setParameter("studentname", student.getStudentname());
    		
    		query.executeUpdate();
    		
    		tx.commit();
    		
    	}
    }
	
	private ScopedEntityManager getScopedEntityManager()
	{
		return PersistenceManager
				.getInstance()
				.getScopedEntityManagerFactory()
				.createScopedEntityManager();
	}
}
