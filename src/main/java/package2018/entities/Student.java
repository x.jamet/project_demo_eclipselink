package package2018.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Student {
	

    
	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(name="studentname", nullable=false)
    String studentname;

    public Student(String studentname) {
        this.studentname = studentname;
    }
    public Student() {
    	
    }
}