package package2018;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("appplication-path2018")
public class Application extends ResourceConfig{
    public Application() {
        packages("package2018");
    }
}