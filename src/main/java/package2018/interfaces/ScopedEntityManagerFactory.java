package package2018.interfaces;

import javax.persistence.EntityManagerFactory;

public interface ScopedEntityManagerFactory extends EntityManagerFactory {
public ScopedEntityManager createScopedEntityManager();
}